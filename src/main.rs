
pub mod tictactoe_game
{
    #[derive(Clone, Debug)]
    pub struct RunStats {

        // The number of marks in the run.
        num_marks: usize,

        // Marks whether the run can is still eligible for a win.
        run_result: CheckResult,
    }

    impl RunStats {
        fn new() -> RunStats {
            RunStats {
                num_marks: 0,
                run_result: CheckResult::NotFilled,
            }
        }
    }


    #[derive(Debug)]
    pub struct Board {
        values: Vec<Vec<i8>>,

        column_stats: Vec<RunStats>,
        row_stats: Vec<RunStats>,
        diagnol_stats: Vec<RunStats>,

        length: usize,
        num_players: i8,
    }

    pub enum GameCondition {
        InProgress,
        Winner(i8),
        CatsGame,
    }

    #[derive(Clone, Debug, PartialEq, Eq)]
    pub enum CheckResult {
        NotFilled,
        Winner(i8),
        Mixed
    }

    const EmptyPlace : i8 = -1;
    const NoRunSeen : i8 = -2;

    impl Board {
        pub fn new(length:usize, num_players:i8) -> Board
        {
            let b = Board {
                values: vec![vec![-1; length]; length],

                column_stats: vec![RunStats::new(); length],
                row_stats: vec![RunStats::new(); length],
                diagnol_stats: vec![RunStats::new(); 2],

                length: length,
                num_players: num_players
            };

            return b
        }

        pub fn print_board(&self) {
            println!("Printing board {0}x{0} w/ {1} players", self.length, self.num_players);
            for row in 0..self.length {
                for col in 0..self.length {
                    print!("{:2} ", self.values[row][col])
                }
                println!("")
            }
        }

        pub fn get_loc(&self, row:usize, col:usize) -> i8 {
            return self.values[row][col]
        }

        pub fn get_loc_by_addr(&self, addr:usize) -> i8 {
            let row = addr / self.length;
            let col = addr % self.length;
            return self.values[row][col]
        }

        pub fn is_loc_empty(&self, row:usize, col:usize) -> bool {
            return self.values[row][col] == -1
        }

        pub fn set_loc(&mut self, row:usize, col:usize, val:i8) {
            if !self.is_loc_empty(row,col) {
                panic!("Trying to play in a filled location!")
            }

            self.values[row][col] = val;
            self.column_stats[col].num_marks += 1;
            self.row_stats[row].num_marks += 1;

            if row == col {
                self.diagnol_stats[0].num_marks += 1;
            }
            
            if (self.length-col-1) == row {
                self.diagnol_stats[1].num_marks += 1;
            }
        }

        // // Checks the entire board for a game over condition
        // fn check_board(&self) -> bool {

        // }

        pub fn check_diag(&mut self, which:usize) -> CheckResult {
            match self.diagnol_stats[which].run_result {
                CheckResult::NotFilled => {
                    if self.diagnol_stats[which].num_marks == self.length {
                        let parms = if which == 0 {
                            (0, self.length+1)
                        } else {
                            (self.length-1, self.length-1)
                        };

                        let result = self.check_run(parms.0, parms.1);
                        self.diagnol_stats[which].run_result = result.clone();
                        return result
                    }
                    else {
                        return CheckResult::NotFilled
                    }
                },
                _ => return self.diagnol_stats[which].run_result.clone()
            }
        }

        pub fn check_row(&mut self, row:usize) -> CheckResult {
            match self.row_stats[row].run_result {
                CheckResult::NotFilled => {
                    if self.row_stats[row].num_marks == self.length {
                        let result = self.check_run(row*self.length, 1);
                        self.row_stats[row].run_result = result.clone();
                        return result
                    }
                    else {
                        return CheckResult::NotFilled
                    }
                },
                _ => return self.row_stats[row].run_result.clone()
            }
        }

        pub fn check_col(&mut self, col:usize) -> CheckResult {
            match self.column_stats[col].run_result {
                CheckResult::NotFilled => {
                    if self.column_stats[col].num_marks == self.length {
                        let result = self.check_run(col, self.length);
                        self.column_stats[col].run_result = result.clone();
                        return result
                    }
                    else {
                        return CheckResult::NotFilled
                    }
                },
                _ =>
                    return self.column_stats[col].run_result.clone()
            }
        }

        fn check_run(&self, start_addr:usize, inc:usize) -> CheckResult {
            let curr_val = self.get_loc_by_addr(start_addr);
            let mut curr_addr = start_addr + inc;
            //println!("First val in run at {} is {}", curr_addr, curr_val);
            for _i in 1..self.length {
                let new_val = self.get_loc_by_addr(curr_addr);
                //println!("Next val in run at {} is {}", curr_addr, new_val);
                if curr_val != new_val {
                    return CheckResult::Mixed
                }

                curr_addr = curr_addr + inc;
            }

            if curr_val == EmptyPlace {
                return CheckResult::NotFilled
            }
            else {
                return CheckResult::Winner(curr_val)
            }
        }
    }
}

fn main() {
    println!("A tic tac toe solver in Rust.");

    // let mut b = Board::new(10, 3);
    // b.set_loc(0,0, 1);
    // b.set_loc(0,1, 2);
    // b.set_loc(1,0, 3);
    // b.set_loc(2,2, 0);

    // b.print_board();

    //crate::tests::diag_check_tests();
}


//#[cfg(test)]
mod tests 
{
    use crate::tictactoe_game as tictactoe;

    #[test]
    #[should_panic]
    fn try_playing_same_place_twice() {
        let mut b = tictactoe::Board::new(3, 2);
        b.set_loc(0,0, 1);
        b.set_loc(1,0, 2);
        b.set_loc(0,2, 1);
        b.set_loc(0,0, 1);
    }

    #[test]
    fn should_be_able_to_fill_board() {
        let mut b = tictactoe::Board::new(3, 2);
        b.set_loc(0,0, 1);
        b.set_loc(0,1, 2);
        b.set_loc(0,2, 1);
        b.set_loc(1,0, 2);
        b.set_loc(1,1, 1);
        b.set_loc(1,2, 2);
        b.set_loc(2,0, 1);
        b.set_loc(2,1, 2);
        b.set_loc(2,2, 2);
        b.print_board();
    }

    #[test]
    fn row_check_tests() {
        let mut b = tictactoe::Board::new(3, 2);
        
        assert_eq!(b.check_row(0), tictactoe::CheckResult::NotFilled);
        assert_eq!(b.check_row(1), tictactoe::CheckResult::NotFilled);
        assert_eq!(b.check_row(2), tictactoe::CheckResult::NotFilled);

        b.set_loc(0,0, 1);
        b.set_loc(1,1, 2);
        b.set_loc(2,2, 1);
        
        assert_eq!(b.check_row(0), tictactoe::CheckResult::NotFilled);
        assert_eq!(b.check_row(1), tictactoe::CheckResult::NotFilled);
        assert_eq!(b.check_row(2), tictactoe::CheckResult::NotFilled);

        b.set_loc(0,1, 1);
        b.set_loc(0,2, 2);
        
        assert_eq!(b.check_row(0), tictactoe::CheckResult::Mixed);
        assert_eq!(b.check_row(1), tictactoe::CheckResult::NotFilled);
        assert_eq!(b.check_row(2), tictactoe::CheckResult::NotFilled);

        b.set_loc(1,0, 2);
        b.set_loc(1,2, 2);

        assert_eq!(b.check_row(0), tictactoe::CheckResult::Mixed);
        assert_eq!(b.check_row(1), tictactoe::CheckResult::Winner(2));
        assert_eq!(b.check_row(2), tictactoe::CheckResult::NotFilled);
    }
    
    #[test]
    fn col_check_tests() {
        let mut b = tictactoe::Board::new(3, 2);
        
        assert_eq!(b.check_col(0), tictactoe::CheckResult::NotFilled);
        assert_eq!(b.check_col(1), tictactoe::CheckResult::NotFilled);
        assert_eq!(b.check_col(2), tictactoe::CheckResult::NotFilled);

        b.set_loc(0,0, 1);
        b.set_loc(0,1, 2);
        b.set_loc(0,2, 1);
        
        assert_eq!(b.check_col(0), tictactoe::CheckResult::NotFilled);
        assert_eq!(b.check_col(1), tictactoe::CheckResult::NotFilled);
        assert_eq!(b.check_col(2), tictactoe::CheckResult::NotFilled);

        b.set_loc(1,0, 1);
        b.set_loc(2,0, 2);
        
        assert_eq!(b.check_col(0), tictactoe::CheckResult::Mixed);
        assert_eq!(b.check_col(1), tictactoe::CheckResult::NotFilled);
        assert_eq!(b.check_col(2), tictactoe::CheckResult::NotFilled);

        b.set_loc(1,1, 2);
        b.set_loc(2,1, 2);

        assert_eq!(b.check_col(0), tictactoe::CheckResult::Mixed);
        assert_eq!(b.check_col(1), tictactoe::CheckResult::Winner(2));
        assert_eq!(b.check_col(2), tictactoe::CheckResult::NotFilled);
    }

    #[test]
    fn diag_check_tests() {
        let mut b = tictactoe::Board::new(3, 2);
        
        assert_eq!(b.check_diag(0), tictactoe::CheckResult::NotFilled);
        assert_eq!(b.check_diag(1), tictactoe::CheckResult::NotFilled);

        b.set_loc(0,0, 1);
        b.set_loc(1,1, 2);
        b.set_loc(2,2, 1);

        assert_eq!(b.check_diag(0), tictactoe::CheckResult::Mixed);
        assert_eq!(b.check_diag(1), tictactoe::CheckResult::NotFilled);

        b.set_loc(0,2, 2);
        b.set_loc(2,0, 2);

        b.print_board();
        println!("check_diag: {:?}", b.check_diag(1));

        assert_eq!(b.check_diag(0), tictactoe::CheckResult::Mixed);
        assert_eq!(b.check_diag(1), tictactoe::CheckResult::Winner(2));
    }
}